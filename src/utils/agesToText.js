import languages from '../constants/language';

export default function agesToText(age, language) {
  if (language === languages.en.value) {
    return age === 1 ? '1 year' : `${age} years`;
  }
  let count = age % 100;
  if (count >= 5 && count <= 20) {
    return `${age} лет`;
  } else {
    count = count % 10;
    if (count === 1) {
      return '1 год';
    } else if (count >= 2 && count <= 4) {
      return `${age} года`;
    } else {
      return `${age} лет`;
    }
  }
}

