export default function isEmptyObject(obj) {
  return !obj || (obj && Object.keys(obj).length === 0);
}
