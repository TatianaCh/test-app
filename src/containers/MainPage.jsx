import React, { Component } from 'react';
import Layout from '../components/Layout';

class MainPage extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    fetch('data/data.json')
      .then(res => res.json())
      .then(json => {
        this.setState({ data: json });
      });
  }

  render() {
    const { data } = this.state;
    if (!data.length) {
      return (<div>Loading</div>);
    }
    return (
      <Layout data={data} />
    );
  }
}

export default MainPage;
