import React from 'react';
import PropTypes from 'prop-types';
import { Player, BigPlayButton, PlayToggle, ControlBar } from 'video-react';
import './VideoBlock.css';

class VideoBlock extends React.Component {
  static propTypes = {
    id: PropTypes.any.isRequired,
    url: PropTypes.string.isRequired,
    className: PropTypes.string,
    onChangePlayingVideo: PropTypes.func,
    onClick: PropTypes.func,
    isPlaying: PropTypes.bool,
  };

  static get defaultProps() {
    return {
      className: '',
      onChangePlayingVideo: null,
      onClick: null,
      isPlaying: false,
    };
  }

  constructor(props) {
    super(props);
    this.scrollHandler = this.scrollHandler.bind(this);
    this.onClickHandler = this.onClickHandler.bind(this);
    this.playPromise = null;
  }

  componentDidMount() {
    window.addEventListener('scroll', this.scrollHandler);
    this.scrollHandler();
  }

  componentWillReceiveProps(nextProps) {
    if (this.refs.player) {
      if (nextProps.isPlaying) {
        this.playPromise = this.refs.player.play();
      } else {
        if (this.playPromise) {
          this.playPromise.then(() => {
            this.refs.player.pause();
          });
        }
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollHandler);
  }

  scrollHandler() {
    const { id, onChangePlayingVideo } = this.props;
    const playerRect = this.playerContainer.getBoundingClientRect();
    const playerTop = playerRect.top + window.scrollY;
    const playerBottom = playerRect.top + playerRect.height + window.scrollY;
    let windowSidePart = document.documentElement.clientHeight / 6;
    if (windowSidePart * 4 < playerRect.height) {
      windowSidePart = (document.documentElement.clientHeight - playerRect.height) / 2;
    }
    const centerTopWithScroll = windowSidePart + window.pageYOffset;
    const centerBootomWithScroll = (document.documentElement.clientHeight + window.pageYOffset) - windowSidePart;
    if (onChangePlayingVideo) {
      if (playerTop < centerBootomWithScroll && playerTop > centerTopWithScroll &&
        playerBottom < centerBootomWithScroll && playerBottom > centerTopWithScroll) {
        onChangePlayingVideo(id, true);
      } else {
        onChangePlayingVideo(id, false);
      }
    }
  }

  onClickHandler() {
    const playerState = this.refs.player.getState();
    if (playerState.operation.operation.action === 'play' && this.props.onClick) {
      this.props.onClick();
    }
  }

  render() {
    const { url, className } = this.props;
    return (
      <div className={className} ref={(el) => { this.playerContainer = el; }} onClick={this.onClickHandler}>
        <Player ref="player" src={url}>
          <BigPlayButton position="center" />
          <ControlBar autoHide={false} disableDefaultControls={true}>
            <PlayToggle onClick={this.onClickHandler} />
          </ControlBar>
        </Player>
      </div>
    );
  }
}
export default VideoBlock;
