import React from 'react';
import PropTypes from 'prop-types';
import NodeGroup from 'react-move/NodeGroup';
import { easeExpInOut } from 'd3-ease';
import agesToText from '../../utils/agesToText.js';
import languages from '../../constants/language';
import VideoBlock from '../VideoBlock';
import './Preview.scss';

class Preview extends React.Component {
  static propTypes = {
    data: PropTypes.array,
    language: PropTypes.string,
    isMobile: PropTypes.bool,
  };

  static get defaultProps() {
    return {
      data: [],
      language: languages.en.value,
      isMobile: false,
    };
  }

  constructor(props) {
    super(props);
    this.onChangePlayingVideo = this.onChangePlayingVideo.bind(this);
    this.onVideoPlayClick = this.onVideoPlayClick.bind(this);
    this.generatePreviewItem = this.generatePreviewItem.bind(this);
    this.state = {
      playingVideoId: '',
      isAutoplay: !props.isMobile,
      data: [...props.data],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.language !== this.props.language) {
      return;
    } else {
      this.setState({data: [...nextProps.data]});
    }
  }

  onChangePlayingVideo(id, isPlay) {
    const { playingVideoId } = this.state
    let newId = playingVideoId;
    if (id === playingVideoId && !isPlay) {
      newId = '';
    } else if (playingVideoId !== id && isPlay) {
      newId = id;
    }
    this.setState({
      playingVideoId: newId,
    });
  }

  onVideoPlayClick() {
    this.setState({
      isAutoplay: false,
      playingVideoId: '',
    });
  }

  generatePreviewItem(item, style={}) {
    const { language } = this.props;
    const { playingVideoId, isAutoplay } = this.state;
    const starClassName = item.favourite ? 'active' : '';
    const containerClassName = item.video ? 'preview-item video-item' : 'preview-item';
    const videoBlockProps = {
      id: item.id,
      url: `data/videos/${item.video}.mp4`,
      className: 'videoContainer',
      isPlaying: isAutoplay && playingVideoId === item.id,
    };
    if (isAutoplay) {
      videoBlockProps.onClick = this.onVideoPlayClick;
      videoBlockProps.onChangePlayingVideo = this.onChangePlayingVideo;
    }
    const videoBlock = (<VideoBlock {...videoBlockProps} />);
    return (
      <div
        className={containerClassName}
        key={item.id}
        style={style}
      >
        <div className="userData">
          <div className="mainUserInfo">
            <div className="avatar">
              <img src={`data/images/${item.image}.svg`} alt={item.name} />
            </div>
            <div className="userName">{item.name}</div>
            <div className={starClassName}>
              <svg>
                <use xlinkHref="#icon-star" />
              </svg>
            </div>
          </div>
          <div className="agesText">{agesToText(item.age, language)}</div>
          <div>{item.phone}</div>
          <p className="aboutUser">{item.phrase}</p>
        </div>
        {item.video && videoBlock}
      </div>
    )
  }

  render() {
    const { isMobile } = this.props;
    const { data } = this.state;

    if (isMobile) {
      return (
        <div className="previews-container">
          {data.map(item => (this.generatePreviewItem(item)))}
        </div>
      )
    }

    return (
      <NodeGroup
        data={data}
        keyAccessor={(d) => d.id}
        start={() => ({
          opacity: 1e-6,
          translateX: 1e-6,
        })}

        enter={(data, index) => ([
          {
            opacity: [0.5, 1],
            timing: { delay: 150 * index, duration: 300 },
          },
          {
            translateX: [2, 0],
            translateY: [-1, 0],
            timing: { delay: 150 * index, duration: 300, ease: easeExpInOut },
          },
        ])}

        update={(data, index) => ({
          opacity: [1],
          translateX: [2, 0],
          translateY: [-1, 0],
          timing: { delay: 150 * index, duration: 300, ease: easeExpInOut },
        })}

        leave={() => ({
        })}
      >
        {(nodes) => (
          <div className="previews-container">
            {nodes.map((nodeItem) => {
              const item = nodeItem.data;
              const itemStyleState = nodeItem.state;
              const itemStyle = {
                opacity: itemStyleState.opacity,
                transform: `translate(${100 * itemStyleState.translateX}%, ${100 * itemStyleState.translateY}%)`
              };
              return this.generatePreviewItem(item, itemStyle);
            })
          }
          </div>
        )}
      </NodeGroup>
    );
  }
}
export default Preview;
