import React from 'react';
import PropTypes from 'prop-types';
import './Switch.scss';

class Switch extends React.Component {
  static propTypes = {
    options: PropTypes.array.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    className: PropTypes.string,
    id: PropTypes.string,
  };

  static get defaultProps() {
    return {
      className: '',
      id: 'switch_component',
    };
  }

  render() {
    const { options, value, className, id, onChange } = this.props;
    let selected = 0;
    const labelWidth = 100 / options.length;
    const optionsList = options.map((item, index) => {
      const isSelected = value === item.value;
      if (isSelected) {
        selected = index;
      }
      const elemId = `${id}-${item.value}`;
      const itemClassName = isSelected ? "switchOptionContainer active" : "switchOptionContainer";
      return (
        <div key={item.value} style={{ width: `${labelWidth}%` }} className={itemClassName} >
          <input
            type="radio"
            name={`${id}-choice`}
            id={elemId}
            className="switch-option"
            checked={isSelected}
            onChange={onChange(item.value)}
          />
          <label htmlFor={elemId}>{item.label}</label>
        </div>
      );
    });
    const switchPosition = (selected * 100) / options.length;
    const switchStyles = {
      left: `calc(${switchPosition}% + 1px)`,
      width: `calc(${labelWidth}% - 1px)`,
    };
    return (
      <div className={className}>
        <div className="switch">
          {optionsList}
          <span className="switchValue" style={switchStyles} />
        </div>
      </div>
    );
  }
}
export default Switch;
