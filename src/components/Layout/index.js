import React, { Component } from 'react';
import PropTypes from 'prop-types';
import viewTypes from '../../constants/viewType';
import sortDirections from '../../constants/sortDirection';
import sortProperties from '../../constants/sortProperties';
import toolbarLabels from '../../constants/toolbarLabels';
import languages from '../../constants/language';
import Switch from '../Switch';
import Table from '../Table';
import Preview from '../Preview';
import icons from './assets/svg-icons';
import './Layout.scss';

class Layout extends Component {
  static propTypes = {
    data: PropTypes.array,
  };

  static get defaultProps() {
    return {
      data: [],
    };
  }

  constructor(props) {
    super(props);
    this.filterUsers = this.filterUsers.bind(this);
    this.onKeyUpHandler = this.onKeyUpHandler.bind(this);
    this.isMobile = this.detectMobile();
    this.data = [...props.data];
    this.state = {
      viewType: viewTypes.table.value,
      sortDirection: sortDirections.asc.value,
      sortProperty: sortProperties.name.value,
      language: languages.en.value,
      increment: 0,
    };
    this.sortData(this.state.sortDirection, this.state.sortProperty);
  }

  onChangeViewType = (value) => () => {
    this.setState({
      viewType: value,
    });
  }

  onChangeLanguage = (value) => () => {
    this.setState({
      language: value,
    });
  }

  onChangeSortDirection = (value) => () => {
    this.sortData(value, this.state.sortProperty);
    this.setState({
      sortDirection: value,
    });
  }

  onChangeSortProperty = (value) => () => {
    this.sortData(this.state.sortDirection, value);
    this.setState({
      sortProperty: value,
    });
  }

  sortByName(value1, value2) {
    const textA = value1.name.toUpperCase();
    const textB = value2.name.toUpperCase();
    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
  }

  sortByAge(value1, value2) {
    return (value1.age < value2.age) ? -1 : (value1.age > value2.age) ? 1 : 0;
  }

  sortById(value1, value2) {
    return (value1.id < value2.id) ? -1 : (value1.id > value2.id) ? 1 : 0;
  }

  sortData(sortDirection, sortProperty) {
    if (sortDirection === sortDirections.asc.value) {
      if (sortProperty === sortProperties.name.value) {
        this.data.sort(this.sortByName);
      }
      if (sortProperty === sortProperties.id.value) {
        this.data.sort(this.sortById);
      }
      if (sortProperty === sortProperties.age.value) {
        this.data.sort(this.sortByAge);
      }
    } else {
      if (sortProperty === sortProperties.name.value) {
        this.data.sort((value1, value2) => (this.sortByName(value2, value1)));
      }
      if (sortProperty === sortProperties.id.value) {
        this.data.sort((value1, value2) => (this.sortById(value2, value1)));
      }
      if (sortProperty === sortProperties.age.value) {
        this.data.sort((value1, value2) => (this.sortByAge(value2, value1)));
      }
    }
  }

  onKeyUpHandler(event) {
    if (event.keyCode === 13) {
      this.filterUsers();
    }
  }

  detectMobile() { 
    if ( navigator.userAgent.match(/Android/i)
       || navigator.userAgent.match(/webOS/i)
       || navigator.userAgent.match(/iPhone/i)
       || navigator.userAgent.match(/iPad/i)
       || navigator.userAgent.match(/iPod/i)
       || navigator.userAgent.match(/BlackBerry/i)
       || navigator.userAgent.match(/Windows Phone/i)
      ) {
      return true;
    }
    else {
      return false;
    }
  }

  filterUsers() {
    const value = this.searchInput.value.trim();
    if (!value && this.props.data.length === this.data.length) {
      return;
    }
    const nameParts = value.toLowerCase().split(' ');

    const comparingFunction = (item) => {
      const name = item.name.toLowerCase();
      const userNameParts = name.split(' ');
      if (nameParts.length > 1) {
        return ((userNameParts[0].indexOf(nameParts[0]) === 0 && userNameParts[1].indexOf(nameParts[1]) === 0) ||
          (userNameParts[1].indexOf(nameParts[0]) === 0 && userNameParts[0].indexOf(nameParts[1]) === 0));
      }
      return (userNameParts[0].indexOf(nameParts[0]) === 0 || userNameParts[1].indexOf(nameParts[0]) === 0);
    };
    this.data = this.props.data.filter(comparingFunction);
    this.sortData(this.state.sortDirection, this.state.sortProperty);
    this.setState({
      increment: this.state.increment + 1,
    });
  }

  render() {
    const { viewType, sortDirection, language, sortProperty } = this.state;
    const labelField = `label_${language}`;
    const viewTypeOptions = Object.keys(viewTypes).map(key => (
      { label: viewTypes[key][labelField], value: viewTypes[key].value }
    ));
    const languageOptions = Object.keys(languages).map(key => (
      { label: languages[key][labelField], value: languages[key].value }
    ));
    const sortDirectionOptions = Object.keys(sortDirections).map(key => (
      { label: sortDirections[key][labelField], value: sortDirections[key].value }
    ));
    const sortPropertiesOptions = Object.keys(sortProperties).map(key => (
      { label: sortProperties[key][labelField], value: sortProperties[key].value }
    ));
    let contentComponent = (<Table data={this.data} language={language} isMobile={this.isMobile} />);
    if (viewType === viewTypes.preview.value) {
      contentComponent = (<Preview data={this.data} language={language} isMobile={this.isMobile}/>);
    }
    return (
      <div className="pageContent">
        <div className="svgIcons" dangerouslySetInnerHTML={{ __html: icons }} />
        <div className="toolbar">
          <div className="languageSwitcher">
            <span className="toolbarLabel">{toolbarLabels.language[labelField]}:</span>
            <Switch value={language} options={languageOptions} onChange={this.onChangeLanguage} id="language" />
          </div>
          <div className="toolbarRow">
            <div className="sortSwitchers">
              <span className="toolbarLabel">{toolbarLabels.sort[labelField]}</span>
              <Switch value={sortProperty} options={sortPropertiesOptions} onChange={this.onChangeSortProperty} id="sortProperty" />
              <Switch value={sortDirection} options={sortDirectionOptions} onChange={this.onChangeSortDirection} id="sortDirection" />
            </div>
            <div className="viewSwitcher">
              <span className="toolbarLabel">{toolbarLabels.view[labelField]}</span>
              <Switch value={viewType} options={viewTypeOptions} onChange={this.onChangeViewType} id="viewType" className="viewSwitch" />
            </div>
          </div>
          <div className="searchBlock">
            <input type="text" placeholder={toolbarLabels.search[labelField]} ref={(el) => { this.searchInput = el; }} onKeyUp={this.onKeyUpHandler} />
            <button onClick={this.filterUsers}>{toolbarLabels.search[labelField]}</button>
          </div>
        </div>
        {contentComponent}
      </div>
    );
  }
}

export default Layout;
