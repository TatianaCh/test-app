import React from 'react';
import PropTypes from 'prop-types';
import NodeGroup from 'react-move/NodeGroup';
import { easeExpInOut } from 'd3-ease';
import agesToText from '../../utils/agesToText.js';
import languages from '../../constants/language';
import './Table.scss';

class Table extends React.Component {
  static propTypes = {
    data: PropTypes.array,
    language: PropTypes.string,
    isMobile: PropTypes.bool,
  };

  static get defaultProps() {
    return {
      data: [],
      language: languages.en.value,
      isMobile: false,
    };
  }

  constructor(props) {
    super(props);
    this.generateTableRow = this.generateTableRow.bind(this);
    this.state = {
      data: [...props.data],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.language !== this.props.language) {
      return;
    } else {
      this.setState({data: [...nextProps.data]});
    }
  }

  generateTableRow(item, style={}) {
    const { language } = this.props;
    const starClassName = item.favourite ? 'starBlock active' : 'starBlock';
    return (
      <div
        className="listItem"
        key={item.id}
        style={style}
      >
        <div className="avatar" title={item.name}>
          <img src={`data/images/${item.image}.svg`} alt={item.name} />
        </div>
        <div className="userinfoContainer">
          <div className="nameBlock">{item.name}</div>
          <div className="userInfo">{agesToText(item.age, language)}</div>
          <div className="userInfo phoneBlock">{item.phone}</div>
          <div className={starClassName}>
            <svg>
              <use xlinkHref="#icon-star" />
            </svg>
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { isMobile } = this.props;
    const { data } = this.state;

    if (isMobile) {
      return (
        <div className="personList">
          {data.map(item => (this.generateTableRow(item)))}
        </div>
      )
    }

    return (
      <NodeGroup
        data={data}
        keyAccessor={(d) => d.id}
        start={() => ({
          opacity: 1e-6,
          translateX: 1e-6,
        })}

        enter={(data, index) => ([
          {
            opacity: [0.5, 1],
            timing: { delay: 150 * index, duration: 100 },
          },
          {
            translateX: [-1, 0],
            timing: { delay: 150 * index, duration: 300, ease: easeExpInOut },
          },
        ])}

        update={(data, index) => ({
          opacity: [1],
          translateX: [-1, 0],
          timing: { delay: 100 * index, duration: 300, ease: easeExpInOut },
        })}

        leave={() => ({
        })}
      >
        {(nodes) => (
          <div className="personList">
            {nodes.map((nodeItem) => {
              const item = nodeItem.data;
              const itemStyleState = nodeItem.state;
              const itemStyle = {
                opacity: itemStyleState.opacity,
                transform: `translateX(${100 * itemStyleState.translateX}%)`
              };
              return this.generateTableRow(item, itemStyle);
            })}
          </div>
        )}
      </NodeGroup>
    );
  }
}
export default Table;
