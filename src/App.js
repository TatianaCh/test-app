import React, { Component } from 'react';
import MainPage from './containers/MainPage';

class App extends Component {
  render() {
    return (
      <div>
        <MainPage />
      </div>
    );
  }
}

export default App;
