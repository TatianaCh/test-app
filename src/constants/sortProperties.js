export default {
  id: {
  	label_en: 'ID',
  	label_ru: 'ID',
  	value: 'id',
  },
  name: {
    label_en: 'Name',
    label_ru: 'Имя',
    value: 'name',
  },
  age: {
    label_en: 'Age',
    label_ru: 'Возраст',
    value: 'age',
  },
};
