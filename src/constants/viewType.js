export default {
  table: {
  	label_en: 'Table',
  	label_ru: 'Таблица',
  	value: 'table',
  },
  preview: {
  	label_en: 'Preview',
  	label_ru: 'Превью',
  	value: 'preview',
  },
};
