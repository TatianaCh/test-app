export default {
  en: {
  	label_en: 'English',
  	label_ru: 'Английский',
  	value: 'en',
  },
  ru: {
  	label_en: 'Russian',
  	label_ru: 'Русский',
  	value: 'ru',
  },
};
