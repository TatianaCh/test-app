export default {
  asc: {
    label_en: 'ASC',
    label_ru: 'По возрастанию',
    value: 'asc',
  },
  desc: {
    label_en: 'DESC',
    label_ru: 'По убыванию',
    value: 'desc',
  },
};
