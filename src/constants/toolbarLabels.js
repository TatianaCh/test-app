export default {
  view: {
  	label_en: 'View',
  	label_ru: 'Вид',
  },
  sort: {
    label_en: 'Sort',
    label_ru: 'Сортировка',
  },
  language: {
    label_en: 'Choose your language',
    label_ru: 'Выберите язык интерфейса',
  },
  search: {
    label_en: 'Search',
    label_ru: 'Поиск',
  },
};
